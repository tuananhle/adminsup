import Login from '../components/login';
import Home from '../components/home';
import Menu from '../components/menu/menu';
import AddProduct from '../components/product/add'
import AddPageContent from '../components/page_content/add'
import ListProduct from '../components/product/list'
import ListPageContent from '../components/page_content/list'
import CategoryProduct from '../components/product/cate/list'
import CategoryEdit from '../components/product/cate/edit'
import CreateProduct from '../components/product/add'
const routes = [
  {
      name: 'login',
      path: '/login',
      component: Login,
      meta:{
        requiresVisitor:true,
      }
  },
  {
      name: 'home',
      path: '/',
      component: Home,
      meta:{
        requiresAuth:true,
      }
  },
  {
      name:'menu',
      path:'/menu',
      component:Menu,
      meta:{
        requiresAuth:true,
      }
  },
  {
    name:'list_category',
    path:'/product/category',
    component:CategoryProduct
  },
  {
    name:'edit_category',
    path:'/product/category/edit/:id_cate',
    component:CategoryEdit
  },
  {
    name:'addPageContent',
    path:'/add_page_content',
    component:AddPageContent,
    meta:{
      requiresAuth:true,
    }
  },
  {
    name:'listPageContent',
    path:'/list_page_content',
    component:ListPageContent,
    meta:{
      requiresAuth:true,
    }
  },
  {
    name:'listProduct',
    path:'/product',
    component:ListProduct,
    meta:{
      requiresAuth:true,
    }
  },
  {
    name:'createProduct',
    path:'/product/create',
    component:CreateProduct,
    meta:{
      requiresAuth:true,
    }
  },
];
export default routes