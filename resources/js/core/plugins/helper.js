import Vue from 'vue';

Vue.prototype.loadingPage = () => {
    this.$store.dispatch("loadings", true)
    setInterval(() => {
      this.$store.dispatch("loadings", false)
    }, 1500)
}