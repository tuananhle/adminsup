import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import modules from './modules'
Vue.use(Vuex)
axios.defaults.baseURL = 'http://127.0.0.1:8080/'

export const store = new Vuex.Store({
    modules,
    state: {
        loading: false,
        token: localStorage.getItem('access_token') || null,
        menu: [],
        allMenu: [],
        editMenu: [],
        active: false,
    },
    getters: {
        loggedIn(state) {
            return state.token !== null;
        }
    },
    mutations: {
        set_loading(state, bol) {
            state.loading = bol;
        },
        retrieveToken(state, token) {
            state.token = token
        },
        destroy_token(state) {
            state.token = null
        },
        set_list_menu(state, menu) {
            state.menu = menu
        },
        set_all_menu(state, allMenu) {
            state.allMenu = allMenu
        },
        set_edit_menu(state, editMenu) {
            state.editMenu = editMenu
        }
    },
    actions: {
        //page content
        // saveAddPageContent(context, data) {
        //     axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        //     return new Promise((resolve, reject) => {
        //         axios.post('pagecontent/add', {...data }).then(response => {
        //             return resolve(response);
        //         }).catch(error => {
        //             return reject(error);
        //         })
        //     });
        // },
        saveCate(context, params) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.post('api/product/category/add', params).then(response => {
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        //end page content
        // menu
        saveEditMenuById(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.post('api/menu/saveEditMenuById/' + data.id, {...data }).then(response => {
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        deleteMenuById(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.get('api/menu/deleteMenuById/' + data).then(response => {
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        getEditMenuById(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.get('api/menu/getEditMenu/' + data).then(response => {
                    let editMenu = response.data;
                    context.commit('set_edit_menu', editMenu);
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        saveAddNewMenu(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.post('api/menu/addNewMenu', {...data }).then(response => {
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        gitListMenu(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.get('api/menu/listMenu').then(response => {
                    let menu = response.data
                    context.commit('set_list_menu', menu)
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        saveChangeMenu(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.post('api/menu/saveChangeMenu', {
                    ...data
                }).then(response => {
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            })
        },
        getAllMenu(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.get('menu/getAllMenu').then(response => {
                    let allMenu = response.data
                    context.commit('api/set_all_menu', allMenu)
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        // end menu
        destroyToken(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.post('api/logout')
                        .then(response => {
                            localStorage.removeItem('access_token');
                            context.commit('destroy_token');
                            resolve(response);
                        }).catch(error => {
                            localStorage.removeItem('access_token');
                            context.commit('destroy_token');
                            reject(error)
                        })
                })
            }
        },
        loadings(context, bol) {
            context.commit('set_loading', bol);
        },
        login(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.post('api/login', {
                    ...data
                }).then(response => {
                    const token = response.data.access_token;
                    localStorage.setItem('access_token', token);
                    context.commit('retrieveToken', token);
                    resolve(response);
                }).catch(error => {
                    reject(error)
                })
            })
        },
    }
})