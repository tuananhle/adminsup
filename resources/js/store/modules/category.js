export default {
    namespaced: true,
    state: {
        token: localStorage.getItem('access_token') || null,
        cate: []
    },
    getters: {},
    mutations: {
        GET_LIST(state, cate) {
            state.cate = cate
        }
    },
    actions: {
        saveCate(context, params) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.post('api/product/category/add', params).then(response => {
                    return resolve(response);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        listCate(context, params) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.post('api/product/category/list', params).then(response => {
                    const cate = response.data
                    context.commit('GET_LIST', cate);
                    return resolve(response.data);
                }).catch(error => {
                    return reject(error);
                })
            });
        },
        edit(context, params) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.get('api/product/category/edit/' + params).then(response => {
                    return resolve(response.data);
                }).catch(error => {
                    return reject(error);
                })
            });
        }
    }
}