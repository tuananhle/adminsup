const http =  'http';
const serverDomain = 'spotech-web.amela.vn';
const serverApiDomain = '14.225.11.12';
const serverHost = http + '://'+ serverDomain;
const serverApiHost = http + '://'+ serverApiDomain + ':3030';
const baseUrl = serverHost;
const apiUrl = serverApiHost + '/api/client';
const redisHost = '127.0.0.1';
const redisPort = 6379;

export default {
    baseUrl: baseUrl,
    apiUrl: apiUrl,
    token:'auth',
    lang:'lang',
    min_year_graduation : 50,
    locales:['en', 'ja', 'vi'],
    defaultLocale:'ja',
    i18n:{
        cookieName: 'lang'
    },
    local:{
        usernameField: 'email',
        passwordField: 'password'
    },
    facebook:{
        clientID: '378249119508368',
        clientSecret: '1d8c0b11c222e314f768351bf60e812e',
        callbackURL: baseUrl + "/api/auth/facebook/callback",
        profileFields: ['id', 'displayName', 'photos', 'email']
    },
    twitter:{
        clientID: 'CONSUMER_KEY',
        clientSecret: 'CONSUMER_SECRET',
        callbackURL: baseUrl +'/api/auth/twitter/callback',
        profileFields: ['id', 'displayName', 'photos', 'email']
    },
    linkedin: {
        clientID: '77j7fey6silvwm',
        clientSecret: 'ZD5dpF2NJsDQeyQJ',
        callbackURL: baseUrl + "/api/auth/linkedin/callback",
        profileFields: [
            "id",
            "formatted-name",
            "email-address",
            "picture-url",
        ],
        scope: ['r_liteprofile', 'r_emailaddress'],
    },
    session:{
        name: 'spotech',
        secret: 'app_secret',
        resave: false,
        saveUninitialized: true,
        key: 'sid',
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 7 * 52 // 1 years
        },
        /*cookie: {
            secure: true,
            httpOnly: false,
            maxAge: null,
            //path:'/'
        }*/
    },
    redis:{
        host: redisHost,
        port: redisPort,
        ttl : 260
    },
    app:{
        head: {
            title: 'spotech',
            meta: [
                { charset: 'utf-8' },
                { name: 'viewport', content: 'width=device-width, initial-scale=1' },
                { hid: 'description', name: 'description', content: 'spotech project' }
            ],
            link: [
                { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
            ]
        },
        server: {
            port: 3112, // default: 3000
            host: '0.0.0.0', // default: localhost
            //host: localhost, // default: localhost
        }
    },
    socket:{
        broadcaster: 'pusher',
        wsHost: serverDomain,
        wsPort: 6001,
        httpHost: serverDomain,
        httpPort: 3030,
        httpPath:'sockets',
        disableStats: true,
        statsHost: serverDomain,
        key:'name',
        authEndpoint: serverApiHost + '/_conversations/auth',
        apiHost: serverApiHost
    },
    aws: {
        storage: {
            AWS_ACCESS_KEY_ID: 'AKIAXOWERBZI7ZCV6GVO',
            AWS_SECRET_ACCESS_KEY: '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',
            AWS_DEFAULT_REGION: 'us-west-2',
            AWS_BUCKET: 'app-fukuon',
        }
    },
    oneSignal:{
        init: {
            appId: '6f4cce72-c03d-4a9c-a360-7292a841071a',
            allowLocalhostAsSecureOrigin: true,
            welcomeNotification: {
                disable: true
            }
        },
        cdn: true,
        // Use any custom URL
        OneSignalSDK: '//cdn.onesignal.com/sdks/OneSignalSDK.js'
    },
    sentry: {
        dsn: 'https://6eb82f99a2dc4ef7ba06f4292240a3f4@sentry.io/1730915', // Enter your project's DSN here
        config: {}, // Additional config
    },
    vue:{
        config: {
            productionTip: false,
            devtools: true
        }
    }
}
