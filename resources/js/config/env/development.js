const http =  'http';
const serverDomain = '127.0.0.1';
const serverApiDomain = '127.0.0.1';
const serverHost = http + '://'+ serverDomain + ':8080';
const serverApiHost = http + '://'+ serverApiDomain + ':8080';
const baseUrl = serverHost;
const apiUrl = serverApiHost + '/api';
// const ct = require('countries-and-timezones');
// const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
// const nyTimezone = ct.getCountriesForTimezone(timezone);
// console.log(nyTimezone);
export default {
    baseUrl: baseUrl,
    apiUrl: apiUrl,
    token:'auth',
    min_year_graduation : 50,
    lang:'lang',
    locales:['en', 'ja', 'vi'],
    defaultLocale:'ja',
}
