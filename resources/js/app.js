/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.$ = require('jquery')
window.JQuery = require('jquery')

require('../assets/js/misc.js');
window.Vue = require('vue');

const config = require('./config')
window.config = config


import 'bootstrap/dist/css/bootstrap.css';
import App from "./components/App.vue"
import VueRouter from 'vue-router'
import axios from 'axios'
import Vuelidate from 'vuelidate'
import ImageUpload from './components/layouts/upload_image'
import ToggleButton from 'vue-js-toggle-button'
import VModal from 'vue-js-modal'
import { store } from './store/store'
import routes from './router/route'

import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'material-icons/iconfont/material-icons.css';
import '@mdi/font/css/materialdesignicons.css'



Vue.use(Vuesax)
Vue.use(ElementUI);
Vue.use(VueRouter)
Vue.use(axios);
Vue.use(ToggleButton)
Vue.use(VModal)
Vue.use(Vuelidate)
Vue.component('image-upload', ImageUpload);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

const router = new VueRouter({
    // mode:'history',
    routes: routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!store.getters.loggedIn) {
            next({
                name: 'login'
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.requiresVisitor)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (store.getters.loggedIn) {
            next({
                name: 'home'
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    icons: {
        iconfont: 'mdi', // default - only for display purposes
    },
    el: '#app',
    router,
    store: store,
    render: h => h(App)
});