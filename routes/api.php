<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Api','middleware' => 'api'],function(){
	Route::post('login','AuthController@login');
	Route::post('upload-image','AllController@uploadImage');
	Route::post('upload-image-multi','AllController@uploadImageMulti');
	
});
Route::group(['namespace'=>'Api','middleware'=>'auth:api'],function(){
	Route::post('logout','AuthController@logout'); 
	Route::group(['prefix'=>'menu'],function(){
		Route::get('listMenu','MenuController@listMenu');
		Route::get('getAllMenu','MenuController@getAllMenu');
		Route::post('saveChangeMenu','MenuController@saveChangemenu');
		Route::post('addNewMenu','MenuController@addNewMenu');
		Route::get('getEditMenu/{id}','MenuController@getEditMenu');
		Route::post('saveEditMenuById/{id}','MenuController@saveEditMenuById');
		Route::get('deleteMenuById/{id}','MenuController@deleteMenuById');
	});
	Route::group(['prefix'=>'pagecontent'], function(){
		Route::post('add','PageContentController@add');
	});
	Route::group(['prefix'=>'product', 'namespace'=>'Product'], function(){
		Route::group(['prefix'=>'category'], function(){
			Route::post('add','CategoryController@add');
			Route::post('list','CategoryController@list');
			Route::get('edit/{id}','CategoryController@edit');
		});
	});
});
