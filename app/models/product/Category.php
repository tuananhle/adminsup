<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Category extends Model
{
    protected $table = "product_category";
    public function saveCate($request)
    {
        $id = $request->id;
        if($id){
            $query = Category::find($id);
            $query->name = $request->name;
            $query->path = $request->path;
            $query->status = $request->status;
            $query->avatar = $request->avatar;
            $query->save();
            return $query;
        }else{
            $query = new Category();
            $query->name = $request->name;
            $query->path = $request->path;
            $query->status = $request->status;
            $query->avatar = $request->avatar;
            $query->save();
            return $query;
        }
        
    }
}
