<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    public function rule()
    {
        return [
            'title'=>'required|unique:page_contents',
            'content'=>'required',
            'status'=>'required',
            'link'=>'required|unique:page_contents',
            'title_seo'=>'required',
            'description'=>'required'
        ];
    }
    public function savePageContent($request)
    {
        $query = new PageContent();
        $query->title = $request->title;
        $query->content = $request->content;
        $query->status = $request->status;
        $query->link = $request->link;
        $query->title_seo = $request->title_seo;
        $query->description = $request->description;
        $query->save();
    }
}
