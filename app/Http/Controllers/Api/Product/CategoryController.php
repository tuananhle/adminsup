<?php

namespace App\Http\Controllers\Api\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\models\product\Category;

class CategoryController extends Controller
{
    public function add(Request $request, Category $category)
    {
        $data = $category->saveCate($request);
        return response()->json(['message' => 'success'], 200);
    }
    public function list(Request $request)
    {
        $keyword = $request->keyword;
        if($keyword == ""){
            $data = Category::get();
        }else{
            $data = Category::where('name', 'LIKE', '%'.$keyword.'%')->get()->toArray();
        }
        return response()->json([
            'data' => $data,
            'message' => 'success'
        ]);
    }
    public function edit($id)
    {
        $data = Category::find($id);
        return response()->json([
            'message' => 'success',
            'data' => $data
        ], 200);
    }
}
