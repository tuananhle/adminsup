<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\PageContent;
use App\models\Menu;
use Validator;

class PageContentController extends Controller
{
    public function add(Request $request,PageContent $page_content,Menu $menu)
    {
        $request->validate($page_content->rule());
        $page_content->savePageContent($request);
        return response()->json([
            'message' => 'Success'
        ]);
    }
}
